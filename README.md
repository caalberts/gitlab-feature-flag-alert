# Gitlab::FeatureFlagAlert

Run `bin/console` for an interactive prompt.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab-feature_flag_alert'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install gitlab-feature_flag_alert

To run the report, use this command:

```
feature-flag-alert
```

## Usage

TODO: Write usage instructions here

- Set ENV variables
  - (required) ENV['GITLAB_TOKEN'] should be set to your access token in order to access the API
  - (optional) ENV['CREATE_ISSUE'] should be `true` if you wish to create 1 issue per group
  - (optional) ENV['GITLAB_TRIAGE_PROJECT_ID'] must be set to the project you wish to create the issues in

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/fabiopitino/gitlab-feature-flag-alert

