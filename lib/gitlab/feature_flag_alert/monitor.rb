# frozen_string_literal: true

require "gitlab/feature_flag_alert/feature_flag"
require "gitlab/feature_flag_alert/gitlab_service"
require "gitlab/feature_flag_alert/issue"

module Gitlab
  module FeatureFlagAlert
    class Monitor
      GITLAB_GIT_REPO = 'https://gitlab.com/gitlab-org/gitlab.git'
      WORK_DIR = 'gitlab-repo'

      def run
        update_repository

        current_milestone = gitlab.current_milestone['title']
        puts "Current milestone: #{current_milestone}"

        milestone_overdue = two_milestones_ago(current_milestone)
        puts "Overdue milestone: #{milestone_overdue}"

        flags_by_group = feature_flags_overdue(milestone_overdue).group_by(&:group)

        flags_by_group.each do |group, flags|
          puts "Feature flags overdue for #{group}:"
          flags.each do |flag|
            puts " * #{flag.name} - #{flag.milestone}"
          end

          puts ""

          create_issue(group, flags) if ENV['CREATE_ISSUE'] == 'true'
        end
      end

      private

      def feature_flags
        @feature_flags ||= FeatureFlag.load_all(File.join(WORK_DIR, 'config/feature_flags/**/*.yml'))
      end

      def gitlab
        @gitlab ||= GitlabService.new
      end

      def assignees
        @assignees ||= gitlab.retrieve_assignees
      end

      def update_repository
        `git clone --depth=1 #{GITLAB_GIT_REPO} #{WORK_DIR} || (cd #{WORK_DIR} && git pull origin master)`
      end

      def two_milestones_ago(milestone)
        major, minor = milestone.split('.').map(&:to_i)

        older_major = minor > 1 ? major : major - 1
        older_minor = (0..12).to_a[minor-2]

        [older_major, older_minor].join('.')
      end

      # overdue feature flags are development flags which are enabled by default
      # and older than 2 milestones.
      def feature_flags_overdue(milestone)
        feature_flags.select do |flag|
          flag.type == 'development' &&
            flag.default_enabled &&
            flag.milestone && flag.milestone < milestone
        end
      end

      def prepared_assignees(group)
        # Unknown group
        return if group.nil?

        group = group.dup
        group.slice!("group::")
        group = group.gsub(' ', '_')

        prepared = assignees.dig(group, "backend_engineering_manager")

        # No DRI
        return if prepared.nil?

        prepared.join(' ')
      end

      def create_issue(group, flags)
        issue = Issue.new(group, flags, prepared_assignees(group))
        gitlab.create_issue(issue)
      end
    end
  end
end
